from PyQt5 import QtWidgets, QtCore, QtGui
from sued import Ui_MainWindow
from acerca_de import Ui_Dialog_acerca
from reemplazar import Ui_dialog
import srt
import sys
import magic
import re
import os
from string import Formatter


sys.stderr = open('errorlog.txt', 'w')


class mywindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowFlags(
            QtCore.Qt.Window |
            QtCore.Qt.CustomizeWindowHint |
            QtCore.Qt.WindowTitleHint
        )
        self.setWindowFlag(QtCore.Qt.WindowCloseButtonHint, False)
        self.setWindowFlag(QtCore.Qt.WindowMaximizeButtonHint, False)
        self.setWindowFlag(QtCore.Qt.WindowMinimizeButtonHint, True)
        self.ui.actionAbrir.triggered.connect(self.abrir_archivo)
        self.ui.actionGuardar.triggered.connect(self.guardar)
        self.ui.actionSeleccionar_destino.triggered.connect(self.cargar_proceso)
        self.ui.actionConvertir.triggered.connect(self.convertir)
        self.ui.actionControles.triggered.connect(self.controles)
        self.ui.actionSalir.triggered.connect(self.salir)
        self.ui.actionAcerca_de.triggered.connect(self.acerca_de)
        self.ui.slider.valueChanged.connect(self.mover_slider)
        self.ui.boton_guardar.clicked.connect(self.guardar)
        self.ui.boton_anterior.clicked.connect(self.sub_anterior)
        self.ui.boton_siguiente.clicked.connect(self.sub_siguiente)
        self.ui.slider.valueChanged.connect(self.mover_slider)
        self.ui.boton_escribir.clicked.connect(self.escribir)
        self.ui.ignorar_html.toggled.connect(self.mover_slider)
        self.ui.boton_escribir.setShortcut("Alt+Return")
        self.ui.boton_anterior.setShortcut("Alt+Left")
        self.ui.boton_siguiente.setShortcut("Alt+Right")
        self.ui.boton_guardar.setShortcut("Ctrl+S")
        self.ui.actionBuscar_y_reemplazar.setShortcut("Ctrl+R")
        self.ui.actionGuardar.setShortcut("Ctrl+S")
        self.ui.actionSalir.setShortcut("Ctrl+Q")
        self.ui.actionAbrir.setShortcut("Ctrl+O")
        self.ui.actionSeleccionar_destino.setShortcut("Ctrl+Y")
        self.ui.actionControles.setShortcut("Ctrl+H")
        self.ui.actionBuscar_y_reemplazar.triggered.connect(self.get_value)
        self.ui.slider.setDisabled(True)
        self.subtitles = []
        self.subtitles_modif = []
        self.sub_en_proceso = []
        self.origen = ""
        self.salida = ""
        self.sin_guardar = False

    def salir(self):
        if self.sin_guardar is True:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Question)
            msg.setText("Hay cambios sin guardar. ¿Desea guardarlos ahora?")
            msg.setWindowTitle("Confirmar salir")
            msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            msg.button(msg.Yes).setText("Sí")
            retval = msg.exec_()
            if retval == 16384:
                self.guardar()
        app.quit()

    def acerca_de(self):
        dlg = StartDlgAcerca.launch(self)
        
    def get_value(self):
        if self.origen == "":
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setText("Se debe seleccionar al menos un archivo origen.")
            msg.setWindowTitle("Error")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
            retval = msg.exec_()
        else:
            buscar, reemplazar, ok = InputDialog.getValues()
            if ok is True:
                for i in self.subtitles_modif:
                    i.content = i.content.replace(buscar, reemplazar)
                self.cargar_sub_indiv(self.ui.slider.value())
        
    def controles(self):
        msg = QtWidgets.QMessageBox()
        msg.setText("""ALT + IZQ = Subtítulo anterior
ALT + DER = Subtítulo siguiente
ALT + ENTER = Escribir actual y siguiente

CTRL + R = Reemplazar caracteres en todo el archivo
CTRL + H = Esta ayuda

CTRL + S = Guardar cambios en archivo modificado
CTRL + O = Abrir archivo de origen
CTRL + Y = Seleccionar archivo destino
CTRL + Q = Salir""")
        msg.setWindowTitle("Atajos de teclado")
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        retval = msg.exec_()        
                      
    def escribir(self):
        if self.subtitles:
            if self.ui.texto_editor.toPlainText() or self.ui.texto_editor.toPlainText() != "":
                self.subtitles_modif[self.ui.slider.value()-1].content = self.ui.texto_editor.toPlainText()
            else:
                self.subtitles_modif[self.ui.slider.value() - 1].content = " "
            self.sub_siguiente()
            self.sin_guardar = True

    def guardar(self):
        if self.subtitles:
            if self.salida == "":
                ruta_destino = os.path.join(os.path.dirname(os.path.abspath(self.origen)), os.path.splitext(self.origen)[0] + "_sued.srt")
                self.salida, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Guardar como...", ruta_destino,
                                                               "Achivos de subtitulo (*.srt);;Todos los archivos (*.*)")
            else:
                with open(self.salida, 'w', encoding='utf-8-sig') as file:
                    file.write(srt.compose(self.subtitles_modif, reindex=False, strict=False))
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Information)
                msg.setText("Archivo guardado como " + self.salida)
                msg.setWindowTitle("Subtítulo generado")
                msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
                retval = msg.exec_()
            self.sin_guardar = False

    def sub_anterior(self):
        if self.subtitles:
            if self.ui.slider.value() > 1:
                self.ui.slider.setValue(self.ui.slider.value() - 1)

    def sub_siguiente(self):
        if self.subtitles:
            if self.ui.slider.value() < len(self.subtitles):
                self.ui.slider.setValue(self.ui.slider.value() + 1)

    def cargar_sub_indiv(self, nro):
        if self.ui.ignorar_html.isChecked():
            self.ui.texto_fuente.setPlainText(self.striphtml(self.subtitles[nro-1].content).strip())
            self.ui.texto_editor.setPlainText(self.striphtml(self.subtitles_modif[nro-1].content).strip())
        else:
            self.ui.texto_fuente.setPlainText(self.subtitles[nro-1].content)
            self.ui.texto_editor.setPlainText(self.subtitles_modif[nro-1].content)
        inicio, ms_inicio = self.strfdelta(self.subtitles[nro-1].start)
        fin, ms_fin = self.strfdelta(self.subtitles[nro-1].end)
        self.ui.label_sub_actual_1.setText(inicio + " " + str(ms_inicio) + "µs")
        self.ui.label_sub_actual_2.setText(fin + " " + str(ms_fin) + "µs")
        if self.ui.autoseleccionar.isChecked():
            self.ui.texto_editor.selectAll()
        self.ui.texto_editor.setFocus()

    def mover_slider(self):
        if self.subtitles:
            self.ui.label_indicador.setText(str(self.ui.slider.value()) + "/" + str(len(self.subtitles)) + " subtitulos.")
            self.cargar_sub_indiv(self.ui.slider.value())

    def abrir_archivo(self):
        self.origen, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Abrir archivo...", "",
                                                            "Achivos de subtitulo (*.srt);;Todos los archivos "
                                                            "(*)")
        if self.origen:
            with open(self.origen, 'rb') as file:
                m = magic.Magic(mime_encoding=True)
                subs_orig = file.read()
                encoding = m.from_buffer(subs_orig)
            result = subs_orig.decode(encoding).encode('utf-8-sig').decode('utf-8-sig')
            subtitle_generator = srt.parse(result)
            subtitle_generator_2 = srt.parse(result)
            self.subtitles = list(subtitle_generator)
            self.subtitles_modif = list(subtitle_generator_2)
            self.ui.group_origen.setTitle(os.path.basename(self.origen) + " - " + encoding)
            self.ui.label_indicador.setText(str(len(self.subtitles)) + " subtitulos cargados.")
            self.ui.slider.setDisabled(False)
            self.ui.slider.setMaximum(len(self.subtitles))
            self.ui.slider.setMinimum(1)
            self.ui.slider.setSingleStep(1)
            for i in self.subtitles:
                i.content = re.sub(' +', ' ', i.content.strip())
            for i in self.subtitles_modif:
                i.content = re.sub(' +', ' ', i.content.strip())
            self.sub_en_proceso = []
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Question)
            msg.setText("Archivo " + os.path.basename(self.origen) + " cargado. ¿Elegir archivo destino (para las modificaciones) ahora?")
            msg.setWindowTitle("Confirmar destino ")
            msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            msg.button(msg.Yes).setText("Sí")
            retval = msg.exec_()
            if retval == 16384:
                self.cargar_proceso()

    def striphtml(self, data):
        p = re.compile(r'<.*?>')
        return p.sub('', data)

    def cargar_proceso(self):
        if self.origen != "":
            ruta_destino = os.path.join(os.path.dirname(os.path.abspath(self.origen)),
                                        os.path.splitext(self.origen)[0] + "_sued.srt")
            self.salida, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Abrir archivo...", ruta_destino,
                                                                   "Achivos de subtitulo (*.srt);;Todos los archivos "
                                                                   "(*)")
            if self.salida:
                with open(self.salida, 'rb') as file:
                    m = magic.Magic(mime_encoding=True)
                    subs_proceso = file.read()
                    encoding = m.from_buffer(subs_proceso)
                result_proc = subs_proceso.decode(encoding).encode('utf-8-sig').decode('utf-8-sig')
                generator_proceso = srt.parse(result_proc)
                self.sub_en_proceso = list(generator_proceso)
                if len(self.sub_en_proceso) != len(self.subtitles_modif):
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Warning)
                    msg.setText("El archivo seleccionado tiene distinta cantidad de subtítulos que el archivo de origen.")
                    msg.setWindowTitle("Error")
                    msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
                    retval = msg.exec_()
                else:
                    for i in self.sub_en_proceso:
                        self.subtitles_modif[i.index - 1].content = i.content
                    self.cargar_sub_indiv(1)
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setText("Se debe seleccionar primero un archivo origen.")
            msg.setWindowTitle("Error")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
            retval = msg.exec_()

    def convertir(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Abrir archivo...", "",
                                                            "Achivos de subtitulo (*.srt);;Todos los archivos "
                                                            "(*)")
        if filename:
            # Convierto a UTF-8-SIG
            with open(filename, 'rb') as file:
                m = magic.Magic(mime_encoding=True)
                subs_orig = file.read()
                encoding = m.from_buffer(subs_orig)
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Question)
            msg.setText("Codificación detectada: " + encoding + "\n¿Convertir a UTF-8?")
            msg.setWindowTitle("Confirmar conversión")
            msg.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            msg.button(msg.Yes).setText("Sí")
            retval = msg.exec_()
            if retval == 16384:
                result = subs_orig.decode(encoding).encode('utf-8-sig').decode('utf-8-sig')
                ruta_destino = os.path.join(os.path.dirname(os.path.abspath(filename)), os.path.basename(filename))
                destino, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Guardar como...", ruta_destino, "Achivos de subtitulo (*.srt);;Todos los archivos (*.*)")
                if destino:
                    with open(destino, 'w', encoding='utf-8-sig') as file:
                        file.write(result)

    def strfdelta(self, tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
        """Convert a datetime.timedelta object or a regular number to a custom-
        formatted string, just like the stftime() method does for datetime.datetime
        objects.

        The fmt argument allows custom formatting to be specified.  Fields can
        include seconds, minutes, hours, days, and weeks.  Each field is optional.

        Some examples:
            '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
            '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
            '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
            '{H}h {S}s'                       --> '72h 800s'

        The inputtype argument allows tdelta to be a regular number instead of the
        default, which is a datetime.timedelta object.  Valid inputtype strings:
            's', 'seconds',
            'm', 'minutes',
            'h', 'hours',
            'd', 'days',
            'w', 'weeks'
        """

        # Convert tdelta to integer seconds.
        if inputtype == 'timedelta':
            remainder = int(tdelta.total_seconds())
        elif inputtype in ['s', 'seconds']:
            remainder = int(tdelta)
        elif inputtype in ['m', 'minutes']:
            remainder = int(tdelta)*60
        elif inputtype in ['h', 'hours']:
            remainder = int(tdelta)*3600
        elif inputtype in ['d', 'days']:
            remainder = int(tdelta)*86400
        elif inputtype in ['w', 'weeks']:
            remainder = int(tdelta)*604800

        microseconds = tdelta.microseconds

        f = Formatter()
        desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
        possible_fields = ('W', 'D', 'H', 'M', 'S')
        constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
        values = {}
        for field in possible_fields:
            if field in desired_fields and field in constants:
                values[field], remainder = divmod(remainder, constants[field])
        return f.format(fmt, **values), microseconds


class StartDlgAcerca(QtWidgets.QDialog, Ui_Dialog_acerca):
    def __init__(self, parent=mywindow):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui_a = Ui_Dialog_acerca()
        self.ui_a.setupUi(self)
        self.setWindowFlags(
            QtCore.Qt.Window |
            QtCore.Qt.CustomizeWindowHint
        )
        self.ui_a.boton_cerrar.clicked.connect(self.close)

    @classmethod
    def launch(cls, parent=None):
        dlg = cls(parent)
        dlg.exec_()
        return None
    
    
class InputDialog(QtWidgets.QDialog, Ui_dialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui_i = Ui_dialog()
        self.ui_i.setupUi(self)
        self.ui_i.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setText("Reemplazar")
        self.ui_i.buttonBox.button(QtWidgets.QDialogButtonBox.Cancel).setText("Cancelar")
        self.ui_i.buttonBox.accepted.connect(self.accept)
        self.ui_i.buttonBox.rejected.connect(self.reject)
        
    def lineas(self):
        buscar = self.ui_i.line_buscar.text()
        reemplazar = self.ui_i.line_reemplazar.text()
        return buscar, reemplazar

    # static method to create the dialog and return (date, time, accepted)
    @staticmethod
    def getValues(parent = None):
        dialog = InputDialog(parent)
        result = dialog.exec_()
        buscar, reemplazar = dialog.lineas()
        return (buscar, reemplazar, result == QtWidgets.QDialog.Accepted)            



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    application = mywindow()
    application.show()
    ret = app.exec_()
    # Cleanup

    sys.stderr.close()
    sys.stderr = sys.__stderr__
    sys.exit(ret)
